### Project Course365

**Mô tả dự án:**
Course365 là một nền tảng học tập trực tuyến cung cấp nhiều khóa học đa dạng. Dự án này xây dựng một hệ thống quản lý khóa học cho Course365, cho phép người dùng duyệt, tìm kiếm, thêm, cập nhật và xóa khóa học. Giao diện người dùng cung cấp trải nghiệm trực quan và dễ sử dụng, cung cấp các khóa học phổ biến và xu hướng.

**Công nghệ sử dụng:**
- **Frontend:**
  - HTML
  - CSS (Bootstrap)
  - JavaScript (jQuery)
  - Font Awesome
  - DataTables
- **Backend:**
  - RESTful API
- **Tools:**
  - MockAPI (mock server)

### Giải thích code các method đã viết

#### `createCourse`
Phương thức này tạo một khóa học mới trong hệ thống.

- **Mô tả:** Thu thập dữ liệu từ form, kiểm tra tính hợp lệ, gửi yêu cầu POST để thêm khóa học mới.
- **Các bước:**
  1. Thu thập dữ liệu từ form thông qua hàm `getNewDataCourse`.
  2. Kiểm tra tính hợp lệ của dữ liệu với hàm `validateNewDataCourse`.
  3. Gửi yêu cầu POST tới API với dữ liệu hợp lệ.
  4. Xử lý phản hồi từ API và cập nhật giao diện người dùng.

#### `getAllCourses`
Phương thức này lấy danh sách tất cả các khóa học.

- **Mô tả:** Gửi yêu cầu GET tới API để lấy dữ liệu khóa học và hiển thị lên bảng.
- **Các bước:**
  1. Gửi yêu cầu GET tới API.
  2. Xử lý phản hồi từ API và điền dữ liệu vào bảng sử dụng DataTables.

#### `getCourseById`
Phương thức này lấy thông tin chi tiết của một khóa học dựa trên ID.

- **Mô tả:** Lấy thông tin của khóa học cụ thể từ API để hiển thị hoặc chỉnh sửa.
- **Các bước:**
  1. Gửi yêu cầu GET tới API với ID của khóa học.
  2. Hiển thị thông tin khóa học trong form chỉnh sửa.

#### `updateCourseById`
Phương thức này cập nhật thông tin của một khóa học dựa trên ID.

- **Mô tả:** Thu thập dữ liệu từ form chỉnh sửa, kiểm tra tính hợp lệ, gửi yêu cầu PUT để cập nhật khóa học.
- **Các bước:**
  1. Thu thập dữ liệu từ form chỉnh sửa.
  2. Kiểm tra tính hợp lệ của dữ liệu với hàm `validateNewDataCourse`.
  3. Gửi yêu cầu PUT tới API với dữ liệu cập nhật.
  4. Xử lý phản hồi từ API và cập nhật giao diện người dùng.

#### `deleteCourseById`
Phương thức này xóa một khóa học dựa trên ID.

- **Mô tả:** Gửi yêu cầu DELETE tới API để xóa khóa học.
- **Các bước:**
  1. Lấy ID của khóa học cần xóa.
  2. Gửi yêu cầu DELETE tới API với ID khóa học.
  3. Xử lý phản hồi từ API và cập nhật giao diện người dùng.

---

Bằng cách sử dụng các phương thức này, hệ thống Course365 có thể quản lý khóa học một cách hiệu quả, cho phép người dùng thực hiện các thao tác thêm, xem, sửa và xóa khóa học một cách dễ dàng.