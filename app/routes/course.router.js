const express = require('express');
const router = express.Router();
const courseController = require('../controllers/course.controller');

// Route để tạo mới một khóa học
router.post('/', courseController.createCourse);

// Route để lấy danh sách tất cả các khóa học
router.get('/', courseController.getAllCourses);

// Route để lấy thông tin khóa học theo ID
router.get('/:id', courseController.getCourseById);

// Route để xóa khóa học theo ID
router.delete('/:id', courseController.deleteCourseById);

router.put('/:id', courseController.updateCourseById);

module.exports = router;
