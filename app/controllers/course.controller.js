const Course = require('../models/course.model');
const mongoose = require('mongoose');

// Hàm tạo mới một khóa học
const createCourse = async (req, res) => {
    try {
        // B1: Lấy dữ liệu từ request body
        const {
            courseCode,
            courseName,
            price,
            discountPrice,
            duration,
            level,
            coverImage,
            teacherName,
            teacherPhoto,
            isPopular,
            isTrending
        } = req.body;

        // B2: Validate dữ liệu
        if (!courseCode) {
            return res.status(400).json({ message: 'courseCode is required' });
        }
        if (!courseName) {
            return res.status(400).json({ message: 'courseName is required' });
        }
        if (price == null || price < 0) {
            return res.status(400).json({ message: 'price is required and must be non-negative' });
        }
        if (discountPrice == null || discountPrice < 0) {
            return res.status(400).json({ message: 'discountPrice is required and must be non-negative' });
        }
        if (!duration) {
            return res.status(400).json({ message: 'duration is required' });
        }
        if (!level) {
            return res.status(400).json({ message: 'level is required' });
        }
        if (!coverImage) {
            return res.status(400).json({ message: 'coverImage is required' });
        }
        if (!teacherName) {
            return res.status(400).json({ message: 'teacherName is required' });
        }
        if (!teacherPhoto) {
            return res.status(400).json({ message: 'teacherPhoto is required' });
        }

        // B3: Thao tác với CSDL
        const newCourse = new Course({
            courseCode,
            courseName,
            price,
            discountPrice,
            duration,
            level,
            coverImage,
            teacherName,
            teacherPhoto,
            isPopular: isPopular || true,
            isTrending: isTrending || false
        });

        const savedCourse = await newCourse.save();

        res.status(201).json({
            message: 'Khóa học được tạo thành công',
            data: savedCourse
        });
    } catch (error) {
        console.error('Error creating course:', error);
        res.status(500).json({
            message: 'Lỗi khi tạo khóa học',
            error: error.message
        });
    }
};

// Hàm lấy danh sách tất cả các khóa học
const getAllCourses = async (req, res) => {
    try {
        const courses = await Course.find();
        res.status(200).json({
            message: 'Lấy danh sách tất cả các khóa học thành công',
            data: courses
        });
    } catch (error) {
        console.error('Error getting courses:', error);
        res.status(500).json({
            message: 'Lỗi khi lấy danh sách khóa học',
            error: error.message
        });
    }
};

// Hàm lấy thông tin khóa học theo ID
const getCourseById = async (req, res) => {
    try {
        const courseId = req.params.id;

        if (!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(400).json({ message: 'courseId is not valid' });
        }

        const course = await Course.findById(courseId);

        if (!course) {
            return res.status(404).json({
                message: 'Khóa học không tồn tại'
            });
        }

        res.status(200).json({
            message: 'Lấy thông tin khóa học thành công',
            data: course
        });
    } catch (error) {
        console.error('Error getting course by ID:', error);
        res.status(500).json({
            message: 'Lỗi khi lấy thông tin khóa học',
            error: error.message
        });
    }
};

// Hàm xóa khóa học theo ID
const deleteCourseById = async (req, res) => {
    try {
        const courseId = req.params.id;

        if (!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(400).json({ message: 'courseId is not valid' });
        }

        const deletedCourse = await Course.findByIdAndDelete(courseId);

        if (!deletedCourse) {
            return res.status(404).json({
                message: 'Khóa học không tồn tại'
            });
        }

        res.status(200).json({
            message: 'Xóa khóa học thành công',
            data: deletedCourse
        });
    } catch (error) {
        console.error('Error deleting course by ID:', error);
        res.status(500).json({
            message: 'Lỗi khi xóa khóa học',
            error: error.message
        });
    }
};
const updateCourseById = async (req, res) => {
    try {
        const courseId = req.params.id;  // Lấy courseId từ route parameter
        const updates = req.body;  // Lấy dữ liệu cập nhật từ request body

        // Validate
        if (!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(400).json({
                status: 'Bad Request',
                message: 'Invalid course ID'
            });
        }

        // Tìm và cập nhật khóa học
        const updatedCourse = await Course.findByIdAndUpdate(courseId, updates, { new: true });

        // Nếu không tìm thấy khóa học với ID cung cấp
        if (!updatedCourse) {
            return res.status(404).json({
                status: 'Not Found',
                message: 'Course not found'
            });
        }

        return res.status(200).json({
            message: 'Course updated successfully',
            data: updatedCourse
        });
    } catch (error) {
        console.error('Error updating course:', error);
        return res.status(500).json({
            message: 'Internal Server Error',
            error: error.message
        });
    }
};
module.exports = {
    createCourse,
    getAllCourses,
    getCourseById,
    deleteCourseById,
    updateCourseById
};
