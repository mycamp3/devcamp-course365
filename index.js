const express = require('express');
const app = express();
const port = 8000;
const path = require('path'); 
const mongoose = require('mongoose');

const courseRouter = require('./app/routes/course.router')
// định nghĩa middleware để phục vụ các tệp tĩnh (css, hình ảnh, ...)
app.use(express.static('views'));

// cấu hình sử dụng json
app.use(express.json());

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://localhost:27017/courseDB")
    .then(() => {
        console.log("MongoDB connected");
    })
    .catch((err) => {
        console.log(err);
    });

// định nghĩa route cho trang chủ
app.get('/', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/index.html' );
    res.sendFile(indexPath);
})

app.get('/', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexListPath = path.join(__dirname, './views/indexListCourse.html' );
    res.sendFile(indexListPath);
})

app.use('/api/courses', courseRouter);

// lắng nghe trên cổng 8000
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})