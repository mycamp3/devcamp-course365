


const gCONTENT_TYPE = "application/json;charset=UTF-8";
const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
var gCourses = [
  {
    courseCode: "",
    courseName: "",
    price: "",
    discountPrice: "",
    duration: "",
    level: "",
    coverImage: "",
    teacherName: "",
    teacherPhoto: "",
    isPopular: true,
    isTrending: true
  }
];


function onPageLoading() {
  var vDivCourseRecommended = $("#courses-recommended");
  var vDivCoursePopular = $("#courses-popular");
  var vDivCourseTrending = $("#courses-trending");
  $.ajax({
    url: gBASE_URL + "/courses/",
    type: "GET",
    contentType: gCONTENT_TYPE,
    success: function (paramRes) {
      gCourses = paramRes;
      var vCoursesTrending = gCourses.filter(v => v.isPopular === false);
      var vCoursesPopular = gCourses.filter(v => v.isTrending === false);
      loadCoursesToHTML(vCoursesTrending, vDivCourseTrending);
      loadCoursesToHTML(vCoursesPopular, vDivCoursePopular);
      loadCoursesToHTML(gCourses, vDivCourseRecommended);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}

function loadCoursesToHTML(paramCourseObj, paramDivElement) {
  var vInnerHtml = '';
  for (var bIndex = 0; bIndex < 4; bIndex++) {
    vInnerHtml += displayOneCourseBlock(paramCourseObj[bIndex]);
  }
  paramDivElement.html(vInnerHtml);
}

// hàm tạo ra 1 khối course
function displayOneCourseBlock(paramCourseObj) {
  var vHtml =
    `<div class="col">
  <div class="card">
    <img src="${paramCourseObj.coverImage}" class="card-img-top" alt="...">
    <div class="card-body">
      <div class="title d-flex justify-content-between">
        <p class="fs-6 fw-bold text-primary course-name" style="line-height: 1.5em;
        height: 3em; width: 80%;" >${paramCourseObj.courseName}</p>
      </div>
      <div class="content d-flex justify-content-between">
        <div class="content-info d-flex">
          <span class="d-flex rounded-2 align-items-center" style="width: max-content;">
            <i class="far fa-clock" style="height:15px"></i>
            <p class="px-1 m-0 fs-6" style="height: 20px;">${paramCourseObj.duration}</p>
          </span>
          <span class="d-flex rounded-2 py-1 px-2 ms-1 align-items-center" style="width: max-content;">
            <p class="m-0 fs-6 " style="height: 20px; font-weight: bold">${paramCourseObj.level}</p>
          </span>
        </div>
      </div>
      <div class="d-flex justify-content-start">
        <span class="fw-bold py-1 px-2" style="width: max-content;">$${paramCourseObj.discountPrice}</span>
        <span class="py-1" style="width: max-content; text-decoration: line-through; color:#7d858d;">$${paramCourseObj.price}</span>
      </div>
    </div>
    <div class="card-footer" style="background-color: #f6f7f6; height: max-content">
      <div class="d-flex justify-content-between">
        <span class="">
          <img style="border-radius: 50%; width: 2.5rem;height: 2.5rem;" class="filter-to-white"
            src="${paramCourseObj.teacherPhoto}">
          <span class="px-1">${paramCourseObj.teacherName}</span>
        </span>
        <span>
          <i class="far fa-bookmark" style="margin-top:10px"></i>
        </span>
      </div>
    </div> 
  </div>
    </div>`;
  return vHtml;
}